# coding: utf-8

import re
import csv
import aiohttp
import asyncio
from aiohttp import ClientSession
from aiohttp.errors import ServerDisconnectedError, ClientResponseError
from bs4 import BeautifulSoup


TEMPLATE_URL = 'http://www.epocacosmeticos.com.br/buscapagina?fq={fq}&PS=50&sl=3d564047-8ff1-4aa8-bacd-f11721c3fce6&cc=1&sm=0&PageNumber={page}'


@asyncio.coroutine
def brands_links():
    base_url = 'http://www.epocacosmeticos.com.br/marcas'
    tree = yield from html_tree(base_url)
    print('Pegamos a listagem de marcas')
    return map(lambda a: a['href'], tree.select('.brandFilter a'))


@asyncio.coroutine
def find_fqs(brand_url):
    """
     fq é uma espécie de identificador de marca, utilizado na paginação
    """
    tree = yield from html_tree(brand_url)
    fq = re.search(r'fq=(\w+%3a\d+)', tree.text)
    # temos que garantir que vai ter esse parâmetro
    assert fq
    print('FQ encontrado {}'.format(fq.groups(0)[0]))
    return fq.groups(0)[0]


@asyncio.coroutine
def products(fq):
    page = 1
    p = set()
    while True:
        url = TEMPLATE_URL.format(fq=fq, page=page)
        tree = yield from html_tree(url)
        product_list = tree.select('div.prateleira > ul > li > h3')
        if product_list:
            for product in product_list:
                a = product.find('a')
                name = a['title'].split(' - ')[0]
                title = a['title'].strip()
                link = a['href']
                p.add((name, title , link))
            page += 1
            print('Listando produtos... para fq {}'.format(fq))
        else:
            print('Produtos encontrados')
            return p


@asyncio.coroutine
def html_tree(url):
    content = yield from make_request(url)
    return BeautifulSoup(content, 'html.parser')

session = ClientSession()
@asyncio.coroutine
def make_request(url, max_retries=5):
    n = 0
    while True:
        try:
            response = yield from session.get(url)
            assert response.status == 200
            # colocando a leitura dentro do bloco de try, pois o read
            # faz streaming do conteúdo e a conexão pode ser cortada durante
            content = yield from response.read()
            return content
        except (ServerDisconnectedError, ClientResponseError, AssertionError):
            print('Desconectado ou Erro 500. Renovando sessão')
            #sobrescreve sessão globalmente
            global session
            session = ClientSession()
            if n >= max_retries:
                raise RuntimeError('Conexão problemática. Verifique')
            else:
                n += 1


@asyncio.coroutine
def scrap():
    prods = set()
    fqs = []
    links = yield from brands_links()
    for l in links:
        fq = yield from find_fqs(l)
        p = yield from products(fq)
        prods = prods.union(p)
    return prods

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    prods = loop.run_until_complete(scrap())
    with open('products.csv', 'w', newline='') as fd:
        f = csv.writer(fd, delimiter=';')
        f.writerows(prods)
    print(u'tarã!!')
    loop.close()
