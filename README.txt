Este projeto roda em python 3.4 ou superior


Instalando dependências
-----------------------
pip install -U pip
pip install -r requirements.txt


Rodar o crawler
---------------

python mr_crawler.py


Rodar os testes
---------------
python tests.py
