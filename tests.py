# coding: utf-8

import re
import unittest
import asyncio
import asynctest
import mr_crawler
from aiohttp import ClientSession
from aiohttp.errors import ServerDisconnectedError, ClientResponseError
from pathlib import Path


def content(fixture):
    p = Path(__file__).parent / fixture
    with open(str(p), 'r') as fd:
        return fd.read()

class TestBrandsLinks(asynctest.TestCase):
    maxDiff = None

    @asynctest.patch('mr_crawler.make_request')
    def test_list_brands(self, make_request):
        make_request.return_value = content('fixtures/marcas.html')
        links = self.loop.run_until_complete(mr_crawler.brands_links())
        for link in links:
            self.assertRegexpMatches(link, r'^http://www.epocacosmeticos.com.br/[a-zA-Z0-9-]*$')


class TestHtmlTree(asynctest.TestCase):
    maxDiff = None

    def setUp(self):
        self.patcher = asynctest.patch('mr_crawler.make_request')
        self._make_req = self.patcher.start()
        self._make_req.return_value = '<html></html>'

    def tearDown(self):
        self._make_req.assert_called_with('http://www.epocacosmeticos.com.br/dior')
        self.patcher.stop() #hammertime

    @asynctest.patch('mr_crawler.BeautifulSoup')
    def test_return_tree(self, BeautifulSoup):
        self.loop.run_until_complete(
            mr_crawler.html_tree('http://www.epocacosmeticos.com.br/dior'))
        BeautifulSoup.assert_called_with('<html></html>', 'html.parser')


class TestFindFqs(asynctest.TestCase):
    maxDiff = None

    def setUp(self):
        self.patcher = asynctest.patch('mr_crawler.make_request')
        self._make_req = self.patcher.start()
        self._make_req.return_value = content('fixtures/dior.html')

    def tearDown(self):
        self._make_req.assert_called_with('http://www.epocacosmeticos.com.br/dior')
        self.patcher.stop() #hammertime

    def test_fq_found(self):
        fq = self.loop.run_until_complete(
            mr_crawler.find_fqs('http://www.epocacosmeticos.com.br/dior'))
        self.assertEqual(fq, 'B%3a2000059')

    def test_fq_not_found(self):
        self._make_req.return_value = ''

        with self.assertRaises(AssertionError):
            fq = self.loop.run_until_complete(
                mr_crawler.find_fqs('http://www.epocacosmeticos.com.br/dior'))


class TestFindProducts(asynctest.TestCase):
    maxDiff = None

    @asynctest.patch('mr_crawler.make_request')
    def test_main(self, make_request):
        fq = 'B%3a2000059'
        make_request.side_effect = [content('fixtures/dior_pag_1.html'), content('fixtures/dior_pag_2.html')]

        product_list = self.loop.run_until_complete(
            mr_crawler.products(fq))

        base_url = 'http://www.epocacosmeticos.com.br/buscapagina?fq=B%3a2000059&PS=50&sl=3d564047-8ff1-4aa8-bacd-f11721c3fce6&cc=1&sm=0&'
        self.assertEqual(
            make_request.call_args_list,
            [asynctest.call('{base_url}PageNumber=1'.format(base_url=base_url)),
            asynctest.call('{base_url}PageNumber=2'.format(base_url=base_url))]
        )

        expected = set([
            ("J'adore Eau de Parfum Dior",
            "J'adore Eau de Parfum Dior - Perfume Feminino",
            'http://www.epocacosmeticos.com.br/j-adore-eau-de-parfum-dior-perfume-feminino/p'),
            ('Miss Dior Eau de Parfum Dior',
            'Miss Dior Eau de Parfum Dior - Perfume Feminino',
            'http://www.epocacosmeticos.com.br/miss-dior-eau-de-parfum-dior-perfume-feminino/p')])

        self.assertEqual(product_list, expected)


class TestMakeRequest(asynctest.TestCase):
    def setUp(self):
        self.response_mock = asynctest.mock.CoroutineMock(status=200)
        self.response_mock.read.return_value = 'OK'

        self._session = mr_crawler.session
        self.session_mock = asynctest.mock.Mock(ClientSession)
        self.session_mock.get.return_value = self.response_mock
        mr_crawler.session = self.session_mock

    def tearDown(self):
        mr_crawler.session = self._session

    def test_everything_is_ok(self):
        content = self.loop.run_until_complete(mr_crawler.make_request('http://some.url'))

        self.session_mock.get.assert_called_with('http://some.url')
        self.assertEqual(content, 'OK')

class TestMakeRequestRetry(asynctest.TestCase):
    def setUp(self):
        self.response_mock = asynctest.mock.CoroutineMock(status=200)
        self.response_mock.read.return_value = 'OK'

        self._session = mr_crawler.session
        self.session_mock = asynctest.mock.Mock(ClientSession)
        self.session_mock.get.side_effect = ServerDisconnectedError
        mr_crawler.session = self.session_mock

    def tearDown(self):
        mr_crawler.session = self._session

    def test_retry_server_disconnect(self):
        with asynctest.patch('mr_crawler.ClientSession', asynctest.mock.MagicMock()) as client:
            new_session = asynctest.mock.Mock(ClientSession)
            new_session.get.return_value = self.response_mock
            client.return_value = new_session
            content = self.loop.run_until_complete(mr_crawler.make_request('http://some.url'))

            self.session_mock.get.assert_called_with('http://some.url')
            new_session.get.assert_called_with('http://some.url')
            self.assertEqual(content, 'OK')

    def test_retry_client_error(self):
        self.session_mock.get.side_effect = ClientResponseError
        with asynctest.patch('mr_crawler.ClientSession', asynctest.mock.MagicMock()) as client:
            new_session = asynctest.mock.Mock(ClientSession)
            new_session.get.return_value = self.response_mock
            client.return_value = new_session

            content = self.loop.run_until_complete(mr_crawler.make_request('http://some.url'))

            self.session_mock.get.assert_called_with('http://some.url')
            new_session.get.assert_called_with('http://some.url')
            self.assertEqual(content, 'OK')

    def test_retry_on_server_error(self):
        self.session_mock.get.return_value = asynctest.mock.CoroutineMock(status=500)
        with asynctest.patch('mr_crawler.ClientSession', asynctest.mock.MagicMock()) as client:
            new_session = asynctest.mock.Mock(ClientSession)
            new_session.get.return_value = self.response_mock
            client.return_value = new_session

            content = self.loop.run_until_complete(mr_crawler.make_request('http://some.url'))

            self.session_mock.get.assert_called_with('http://some.url')
            new_session.get.assert_called_with('http://some.url')
            self.assertEqual(content, 'OK')

    def test_quit_when_exceed_all_retries(self):
        self.session_mock.get.return_value = asynctest.mock.CoroutineMock(status=500)
        with asynctest.patch('mr_crawler.ClientSession', asynctest.mock.MagicMock()) as client:
            new_session = asynctest.mock.Mock(ClientSession)
            new_session.get.return_value = asynctest.mock.CoroutineMock(status=500)
            client.return_value = new_session

            with self.assertRaises(RuntimeError):
                self.loop.run_until_complete(mr_crawler.make_request('http://some.url', max_retries=1))



class TestScrap(asynctest.TestCase):

    def setUp(self):
        self.links = [
            'http://www.epocacosmeticos.com.br/dior',
            'http://www.epocacosmeticos.com.br/test'
        ]
        self.fqs = ['B%3a2000059', 'B%3a2000021']
        #para garantir
        self.products = [set([
            ("Test",
            "Test - Test",
            'http://www.epocacosmeticos.com.br/test/p'),
            ('Miss Dior Eau de Parfum Dior',
            'Miss Dior Eau de Parfum Dior - Perfume Feminino',
            'http://www.epocacosmeticos.com.br/miss-dior-eau-de-parfum-dior-perfume-feminino/p')]),

            set([("Test",
            "Test - Test",
            'http://www.epocacosmeticos.com.br/test/p')]),
        ]

    @asynctest.patch('mr_crawler.products')
    @asynctest.patch('mr_crawler.find_fqs')
    @asynctest.patch('mr_crawler.brands_links')
    def test_find_all_products(self, brands, fqs, prods):
        brands.return_value = self.links
        fqs.side_effect = self.fqs
        prods.side_effect = self.products

        products = self.loop.run_until_complete(mr_crawler.scrap())

        self.assertTrue(brands.called)
        self.assertEqual(
            fqs.call_args_list,
            [asynctest.call('http://www.epocacosmeticos.com.br/dior'),
             asynctest.call('http://www.epocacosmeticos.com.br/test')]
        )
        self.assertEqual(
            prods.call_args_list,
            [asynctest.call('B%3a2000059'),
             asynctest.call('B%3a2000021')]
        )
        self.assertTrue(prods.called)

        self.assertEqual(products, set([
            ("Test",
            "Test - Test",
            'http://www.epocacosmeticos.com.br/test/p'),
            ('Miss Dior Eau de Parfum Dior',
            'Miss Dior Eau de Parfum Dior - Perfume Feminino',
            'http://www.epocacosmeticos.com.br/miss-dior-eau-de-parfum-dior-perfume-feminino/p')])
        )


if __name__ == '__main__':
    unittest.main()
